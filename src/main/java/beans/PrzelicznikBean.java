/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PrzelicznikBean extends MainBean{
    
    private double kwota;
    private double wynik;

    private EnumWaluty walutaDocelowa;
    
    private Map<String, String> waluty1 ;
   
    
    
    
    /**
     * @return the kwota
     */
    public double getKwota() {
        return kwota;
    }

    /**
     * @param kwota the kwota to set
     */
    public void setKwota(double kwota) {
        this.kwota = kwota;
    }

    /**
     * @return the wynik
     */
    public double getWynik() {
        return wynik;
    }

    /**
     * @param wynik the wynik to set
     */
    public void setWynik(double wynik) {
        this.wynik = wynik;
    }



    /**
     * @return the walutaDocelowa
     */
    public EnumWaluty getWalutaDocelowa() {
        return walutaDocelowa;
    }

    /**
     * @param walutaDocelowa the walutaDocelowa to set
     */
    public void setWalutaDocelowa(EnumWaluty walutaDocelowa) {
        this.walutaDocelowa = walutaDocelowa;
    }

    /**
     * @return the waluty
     */
    public Map<String, String> getWaluty1() {
        if(waluty1!=null && waluty1.size()>0)
         return waluty1;
        
        
        waluty1=new HashMap<String,String>();
        
        Object[] walutyValues=EnumWaluty.class.getEnumConstants();
        
        for(Object walutaLabel : walutyValues){
        
            waluty1.put(String.valueOf(walutaLabel), "");
        }
        
        waluty1.put("PLN", "selected=\"selected\"");
        
        return waluty1;
    }

    /**
     * @param waluty the waluty to set
     */
    public void setWaluty1(Map<String, String> waluty) {
        this.waluty1 = waluty;
    }

 

   
    
    
    
}
