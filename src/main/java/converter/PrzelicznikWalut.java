/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;

public class PrzelicznikWalut {

    static String kodWaluty, kodWaluty1, kursSredni, stanEUR, stanPLN;
    static Document doc, docStan;
    static File fXmlFile, fXmlFileStan;

    
    public  double przeliczWalute(String kodWaluty1, String kodWaluty2, double kwota1) {
        
        double kursLiczbowy1 = 1, kursLiczbowy2 = 1;
        
        try {
            URL nbp = new URL("http://nbp.pl/kursy/xml/LastA.xml");
            URLConnection urlConn = nbp.openConnection();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc;
            doc = dBuilder.parse(urlConn.getInputStream());
            kodWaluty = kursSredni = " ";
            NodeList nList = doc.getElementsByTagName("pozycja");
            
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String kodWaluty = eElement.getElementsByTagName("kod_waluty").item(0).getTextContent();
                    String przelicznik = eElement.getElementsByTagName("przelicznik").item(0).getTextContent();
                    przelicznik = przelicznik.replace(',', '.');
                    double przelicznikLiczbowy  = (double) Double.parseDouble(przelicznik);
                    
                    if (kodWaluty.equals(kodWaluty1)) {
                        String kursSredni1 = eElement.getElementsByTagName("kurs_sredni").item(0).getTextContent();
                        kursSredni1 = kursSredni1.replace(',', '.');
                        kursLiczbowy1 = (double) Double.parseDouble(kursSredni1) / przelicznikLiczbowy;
                    }
  
                    if (kodWaluty.equals(kodWaluty2)) {
                        String kursSredni2 = eElement.getElementsByTagName("kurs_sredni").item(0).getTextContent();
                        kursSredni2 = kursSredni2.replace(',', '.');
                        kursLiczbowy2 = (double) Double.parseDouble(kursSredni2) / przelicznikLiczbowy;
                    }
                }
            }
            
        } catch (Exception e) {
          e.printStackTrace();
        }

        double wynik = przelicz(kwota1,kursLiczbowy1,kursLiczbowy2);
        return wynik;
    }

    private double przelicz(double kwota1, double kursLiczbowy1, double kursLiczbowy2){
        double wynik = kwota1 * kursLiczbowy1 / kursLiczbowy2;
        wynik *= 10000;
        wynik  = Math.round(wynik);
        wynik /= 10000;
        return wynik;
    }
}
