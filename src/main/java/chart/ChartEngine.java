/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chart;

import beans.EnumWaluty;
import converter.PrzelicznikWalut;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;


public class ChartEngine {
    public static CategoryDataset createDataset(String walutaIn ) {

        final String kurs = "Kurs";        

        final DefaultCategoryDataset dataset = 
          new DefaultCategoryDataset();  

        Object[] walutyValues=EnumWaluty.class.getEnumConstants();

        for(Object walutaLabel : walutyValues) {
            int kwota = 1;
            String waluta = walutaLabel.toString();
            if (waluta.equals("JPY")) {
                kwota = 100;
                waluta = "100 JPY";
            }
            dataset.addValue((new PrzelicznikWalut()).przeliczWalute(walutaLabel.toString(), walutaIn, kwota) , waluta , kurs);   
        }
        
        return dataset; 
   }
}
