/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import beans.EnumWaluty;
import beans.MainBean;
import chart.ChartEngine;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;


public class ChartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
        
        MainBean wb=new MainBean();
        

        if(request.getParameter("walutaIn")!=null){
            
            EnumWaluty walutaIn=EnumWaluty.valueOf(request.getParameter("walutaIn"));
            wb.setWalutaZrodlowa(walutaIn);
            
            utils.Utils.zeroMap(wb.getWaluty2());
            
            wb.getWaluty2().put(walutaIn.toString(),"selected=\"selected\"");
            
            //kod tworzenia wykresu
             try {
                  JFreeChart barChart = ChartFactory.createBarChart("Kursy względem "+walutaIn.toString(),"Waluty","Kurs",ChartEngine.createDataset(walutaIn.toString()), PlotOrientation.VERTICAL,  true, true, false);
            ChartPanel chartPanel = new ChartPanel( barChart );        
      chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );        

    //dodanie zapisu wykresu wzgledem waluty
                 System.out.println(walutaIn.toString());
    OutputStream outFile = new FileOutputStream(System.getProperty("user.home")+"/wykres"+walutaIn.toString()+".jpg");
                ChartUtils.writeChartAsJPEG(outFile, barChart,  chartPanel.getPreferredSize().width,  chartPanel.getPreferredSize().height);
                outFile.close();

} catch (IOException ex) {
 
}
            
            
            
        }
            
        
        request.setAttribute("pb", wb);
        
        request.getRequestDispatcher("wykresy.jsp").forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
