/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import beans.EnumWaluty;
import beans.PrzelicznikBean;
import converter.PrzelicznikWalut;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
            /* TODO output your page here. You may use following sample code. */
            String kwota=request.getParameter("kwota");
            
            
            PrzelicznikBean pb=new PrzelicznikBean();
         
           
           if(request.getParameter("walutaIn")!=null){
            
            EnumWaluty walutaIn=EnumWaluty.valueOf(request.getParameter("walutaIn"));
            
            EnumWaluty walutaOut=EnumWaluty.valueOf(request.getParameter("walutaOut"));
            
                      
            pb.setWalutaDocelowa(walutaOut);
            pb.setWalutaZrodlowa(walutaIn);
            pb.setWynik((new PrzelicznikWalut()).przeliczWalute(walutaIn.toString(), walutaOut.toString(), Double.parseDouble(kwota)));
            pb.setKwota(Double.parseDouble(kwota));
            
            utils.Utils.zeroMap(pb.getWaluty1());
            utils.Utils.zeroMap(pb.getWaluty2());
            
           
            pb.getWaluty1().put(walutaIn.toString(),"selected=\"selected\"");
            pb.getWaluty2().put(walutaOut.toString(),"selected=\"selected\"");
           
            String history=kwota+" "+walutaIn.toString() + " na walutę: "+walutaOut.toString()+" to: "+pb.getWynik()+",\r\n";
            
            File f=new File(System.getProperty("user.home")+"/history.txt");
            if(!f.exists())
                f.createNewFile();
            
            
            Files.write(Paths.get(System.getProperty("user.home")+"/history.txt"),  history.getBytes(), StandardOpenOption.APPEND); 
            
            
           }
           request.setAttribute("pb", pb);
          
            request.getRequestDispatcher("kalkulator.jsp").forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

   

   

}
