<%@page import="beans.MainBean" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.Map.Entry" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Wykresy</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <div class="header">
        <h1>Wykresy walutowe</h1>
        <a href="index.jsp">< Powrót do menu</a>
    </div>
    <div class="content">
        <form class="chart" method="post" action="/ChartServlet">
            <select class="select" name="walutaIn">
                <%
                    Map<String, String> waluty2 = ((MainBean) request.getAttribute("pb")).getWaluty2();
                    for (Entry<String, String> waluta2 : waluty2.entrySet()) {
                %>
                <option value="<%=waluta2.getKey()%>" <%=waluta2.getValue()%>>
                    <%=waluta2.getKey()%>
                </option>
                <%
                    }
                %>
            </select>
            <button class="btn" type="submit">Generuj</button>
        </form>
    </div>
</div>
</body>
</html>
