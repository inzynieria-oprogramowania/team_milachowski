<!docctype html>
<head lang="" pl
">
<meta charset="" utf-8
">
<title>Projekt Waluty</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <div class="header">
        <h1>Projekt waluty</h1>
        <h2>Pliki zapisywane w lokalizacji: <%=System.getProperty("user.home")%>
        </h2>
    </div>
    <div class="content">
        <nav class="nav">
            <ul>
                <li><a href="/MainServlet">Przelicznik walut</a></li>
                <li><a href="/ChartServlet">Wykresy walut</a></li>
            </ul>
        </nav>
    </div>
</div>
</body>
</html>